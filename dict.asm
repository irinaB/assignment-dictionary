global find_word

extern string_equals

; Аргументы:

; Указатель на нуль-терминированную строку. (rdi)
; Указатель на начало словаря. (rsi)

; find_word будет искать ключь по всему соварю. 
; Вернет адрес начала вхождения в случае, если вхождение найдено,
; иначе вернет 0.

find_word:
        push rdi
        push rsi
        add rsi, 8
        call string_equals
        pop rsi
        pop rdi
        cmp rax, 1
        je .exit
        mov r9, [rsi]
        test r9, r9
        jz .abort
        mov rsi, r9
        jmp find_word

    .exit:
        mov rax, rsi
        ret

    .abort:
        xor rax, rax
        ret
