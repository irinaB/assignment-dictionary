NASM = nasm -g -felf64
LD = ld -o

all: lab2

dict.o: dict.asm
	$(NASM) dict.asm

lib.o: lib.asm
	$(NASM) lib.asm

main.o: main.asm words.inc
	$(NASM) main.asm

lab2: dict.o lib.o main.o
	$(LD) lab2 dict.o lib.o main.o

clean:
	rm -f *.o

cleanall:
	rm -f *.o lab2
