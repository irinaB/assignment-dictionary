%xdefine PREV_DICT_PT 0

%macro colon 2
%2:
    dq PREV_DICT_PT
    db %1, 0
    %xdefine PREV_DICT_PT %2
%endmacro
