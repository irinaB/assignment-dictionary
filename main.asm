%define BUFFER_SIZE 256

%include "words.inc"

global _start
extern read_word, exit, print_string, print_newline, string_length, read_char, print_uint
extern find_word
extern print_string_err, print_char_err, print_newline_err

section .data
    fail_text: db "Couldn't find the word!", 0
    err_text: db "An error occurred!", 0

section .text

; Читает строку символов в буфер с stdin.
; Пытается найти вхождение в словаре; если оно найдено, 
; распечатывает в stdout значение по этому ключу. Иначе выдает сообщение об ошибке.
_start:
        sub rsp, BUFFER_SIZE
	    mov rdi, rsp
        mov rsi, BUFFER_SIZE
        call read_string
        test rax, rax
        jz .err

        mov rdi, rsp
        mov rsi, PREV_DICT_PT
        call find_word

        test rax, rax
        jz .fail

        mov rdi, rax
        add rdi, 8
        call string_length
        inc rax
        add rdi, rax
        call print_string

        mov rdi, 0
        call exit

    .err:
        mov rdi, err_text
        jmp .fail_continue

    .fail:
        mov rdi, fail_text

    .fail_continue:
        call print_string_err
        call print_newline_err
        mov rdi, -1
        call exit
